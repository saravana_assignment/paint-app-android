package com.paint.utils

import android.content.Context
import com.paint.utils.SharedPrefConstant.PRIVATE_KEY

class SharedPreferenceUtils(mContext: Context) {


    private val context = mContext

    private val sharedPref = context.getSharedPreferences(
        PRIVATE_KEY, Context.MODE_PRIVATE
    )

    private val editor = sharedPref.edit()

    fun setString(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(key: String): String {
        return sharedPref.getString(
            key, ""
        )?: ""
    }

    fun setInt(key: String, value: Int) {
        editor.putInt(key, value)
        editor.apply()
    }

    fun getInt(key: String): Int{
        return sharedPref.getInt(
            key, 0
        )
    }

    fun setFloat(key: String, value: Float) {
        editor.putFloat(key, value)
        editor.apply()
    }

    fun getFloat(key: String): Float{
        return sharedPref.getFloat(
            key, 0F
        )
    }

    fun setBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(key: String): Boolean {
        return sharedPref.getBoolean(
            key, false
        )
    }
}