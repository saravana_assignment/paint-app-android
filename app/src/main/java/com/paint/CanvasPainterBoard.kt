package com.paint

import android.graphics.Color
import android.view.ViewGroup
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.gestures.transformable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.viewinterop.AndroidView
import com.paint.controller.PainterController
import com.paint.utils.AppUtils.createPath

@Composable
internal fun CanvasPainterBoard(
    painterController: PainterController,
    modifier: Modifier = Modifier
) = AndroidView(modifier = modifier, factory = {

    ComposeView(it).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        setBackgroundColor(Color.WHITE)
        setContent {

                val path = painterController.paintPath.collectAsState()

                // set up all transformation states
                var scale by remember { mutableStateOf(1.0f) }
                var rotation by remember { mutableStateOf(0f) }
                var offset by remember { mutableStateOf(Offset.Zero) }
                val state = rememberTransformableState { zoomChange, offsetChange, rotationChange ->
                    scale *= zoomChange
                    rotation += rotationChange
                    offset += offsetChange
                }

                Canvas(
                    modifier = Modifier
                        // apply other transformations like rotation and zoom
                        // on the pizza slice emoji
                        .graphicsLayer(
                            scaleX = scale,
                            scaleY = scale,
                            rotationZ = rotation,
                            translationX = offset.x,
                            translationY = offset.y
                        )
                        // add transformable to listen to multitouch transformation events
                        // after offset
                        .transformable(state = state)
                        .fillMaxSize()
                        .clipToBounds()

                        .pointerInput(Unit) {
                            detectDragGestures(onDragStart = { offset ->
                                painterController.addPath(offset)
                            }) { change, _ ->
                                painterController.updateLastPath(change.position)
                            }
                        }
                        .drawBehind {
                            if (painterController.getImportBitmap() != null) {
                                val bitmap = painterController.getImportBitmap()!!.asImageBitmap()

                                // Draw the bitmap onto the canvas
                                drawImage(bitmap, topLeft = Offset(0f, 0f), alpha = 1.0f)
                            }
                            for (p in path.value)
                                drawPath(
                                    createPath(p.points),
                                    color = p.color,
                                    style = Stroke(
                                        width = p.stroke,
                                        cap = StrokeCap.Round
                                    ),
                                )

                        }
                ) {

                }
            }
            painterController.updateCanvasPaintView(this)
    }

})


