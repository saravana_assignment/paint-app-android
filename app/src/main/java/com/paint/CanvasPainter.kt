package com.paint

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.paint.components.PainterToolbar
import com.paint.controller.PainterController

@Composable
fun CanvasPainter(
    modifier: Modifier = Modifier,
    painterController: PainterController
) {
//  Paint Section
    Column(modifier) {
        if (painterController.showToolbar)
            PainterToolbar(painterController = painterController)
        CanvasPainterBoard(
            painterController,
            modifier
                .fillMaxWidth()
                .weight(1f)
        )
    }
}