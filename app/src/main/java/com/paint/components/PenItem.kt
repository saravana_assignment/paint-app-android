package com.paint.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.paint.R
import com.paint.models.PaintBrush

@Composable
fun PenItem(modifier: Modifier = Modifier, paintBrush: PaintBrush) {
    Box {
        Image(
            painterResource(if (paintBrush.id == -1) R.drawable.ic_color_lens else R.drawable.ic_brush),
            modifier = modifier.width(if (paintBrush.id == -1) 50.dp else 60.dp)
                .rotate(135f)
                .padding(horizontal = if (paintBrush.id == -1) 10.dp else 0.dp),
            alignment = Alignment.Center,
            contentDescription = null,
            contentScale = ContentScale.Fit,
            colorFilter = if (paintBrush.id == -1) null else ColorFilter.tint(paintBrush.color)
        )
    }
}
