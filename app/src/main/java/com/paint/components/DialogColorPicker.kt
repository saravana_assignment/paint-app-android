package com.paint.components

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.godaddy.android.colorpicker.ClassicColorPicker
import com.godaddy.android.colorpicker.HsvColor
import com.paint.controller.PainterController

@Composable
internal fun DialogColorPicker(controller: PainterController, onDismissed: () -> Unit) {
    var currentColor = HsvColor.DEFAULT
    Dialog(onDismissRequest = {
        onDismissed()
    }, properties = DialogProperties()) {
        ClassicColorPicker(
            color = currentColor,
            modifier = Modifier
                .height(300.dp)
                .padding(16.dp),
            onColorChanged = { hsvColor: HsvColor ->
                controller.setCustomColor(hsvColor.toColor())
            }
        )
    }
}