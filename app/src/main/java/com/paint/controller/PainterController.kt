package com.paint.controller

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Environment
import android.view.View
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.paint.R
import com.paint.models.PaintBrush
import com.paint.models.PaintPath
import com.paint.models.StorageOptions
import com.paint.utils.AppUtils
import com.paint.utils.SharedPrefConstant.PAINT_COLOR
import com.paint.utils.SharedPrefConstant.STROKE_WIDTH
import com.paint.utils.SharedPreferenceUtils
import com.paint.ext.openGallery
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.IOException


@Composable
fun rememberCanvasPainterController(
    activity : Activity,
    maxStrokeWidth: Float = 50f,
    showToolbar: Boolean = true,
    storageOptions: StorageOptions = StorageOptions(),
    color: Color? = null,
    onBitmapGenerated: ((bitmap: Bitmap) -> Unit)? = null,
) = remember {
    PainterController(activity, maxStrokeWidth, showToolbar, storageOptions, onBitmapGenerated).apply {
        color?.let {
            setCustomColor(color)
        }
    }
}

class PainterController(
    val activity : Activity,
    var maxStrokeWidth: Float = 50f,
    var showToolbar: Boolean = true,
    var storageOptions: StorageOptions = StorageOptions(),
    private val onBitmapGenerated: ((bitmap: Bitmap) -> Unit)? = null,
) {

    internal val strokeWidth = MutableStateFlow(5f)
    internal val isStrokeSelection = MutableStateFlow(false)

    internal val paintPath = MutableStateFlow(listOf<PaintPath>())
    internal val undonePath = MutableStateFlow(listOf<PaintPath>())
    internal val selectedColor = MutableStateFlow(AppUtils.PENS[1])
    internal val prevSelectedColor = MutableStateFlow(AppUtils.PENS[1])

    internal var isEraserEnabled = false
    private val canvasPaintView = MutableStateFlow<View?>(null)
    private var importedBitmap: Bitmap? = null

    private val sharedPref = SharedPreferenceUtils(activity)

    init {
        setStrokeWidth(sharedPref.getFloat(STROKE_WIDTH))
    }

    internal fun updateCanvasPaintView(view: View) {
        canvasPaintView.value = view
    }

    internal fun toggleStrokeSelection() {
        isStrokeSelection.value = isStrokeSelection.value.not()
    }

    fun setPaintColor(paintBrush: PaintBrush) {
        sharedPref.setInt(PAINT_COLOR, paintBrush.color.toArgb())
        selectedColor.value = paintBrush
    }

    internal fun addPath(offset: Offset) {
        undonePath.value = emptyList()
        paintPath.update {
            it.toMutableList().apply {
                add(
                    PaintPath(
                        mutableStateListOf(offset),
                        strokeWidth.value,
                        selectedColor.value.color
                    )
                )
            }
        }
    }

    internal fun updateLastPath(offset: Offset) {
        paintPath.update {
            it.apply {
                last().apply {
                    points.add(offset)
                }
            }
        }
    }

    internal fun savePaint() {
        val fileName = "painting_${System.currentTimeMillis()}.png"
        val filePath =
            "${Environment.DIRECTORY_DCIM}/${storageOptions.saveDirectoryName}"
        saveBitmap(filePath, fileName, false)
    }

    internal fun saveBitmap(filePath: String, fileName: String, isShare: Boolean) {
        if (paintPath.value.isEmpty()) {
            return
        }
        CoroutineScope(Dispatchers.IO).launch {
            val view = canvasPaintView.value!!
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            canvas.drawColor(Color.Red.toArgb())
            view.draw(canvas)

            if (storageOptions.shouldSaveByDefault) {
                try {
                    AppUtils.saveBitmap(activity, view.context, bitmap, storageOptions, filePath, fileName, isShare)
                } catch (e: IOException) {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(
                            view.context, "Cannot Save Painting",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                if (!isShare) {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(view.context, "Saved", Toast.LENGTH_LONG)
                            .show()
                    }
                }
            } else {
                onBitmapGenerated?.invoke(bitmap)
                    ?: throw NullPointerException(view.context.getString(R.string.invalid_interface))
            }
        }
    }

    internal fun erase() {
        if(!isEraserEnabled) {
            isEraserEnabled = true
            prevSelectedColor.value = selectedColor.value
            selectedColor.value = PaintBrush(-1, Color.White)
        } else {
            isEraserEnabled = false
            selectedColor.value = prevSelectedColor.value
        }
    }

    internal fun disableEraser() {
        isEraserEnabled = false
        selectedColor.value = prevSelectedColor.value
    }

    internal fun share() {
        val fileName = "painting_${System.currentTimeMillis()}.png"
        val filePath = "${Environment.DIRECTORY_DCIM}/${storageOptions.saveDirectoryName}/${storageOptions.shareDirectoryName}"
        saveBitmap(filePath, fileName, true)
    }

    internal fun loadImage() {
        activity.openGallery()
    }

    fun setStrokeWidth(width: Float) {
        sharedPref.setFloat(STROKE_WIDTH,width)
        strokeWidth.value = width
    }

    fun setCustomColor(color: Color) {
        sharedPref.setInt(PAINT_COLOR, color.toArgb())
            selectedColor.value = PaintBrush(-1, color)
    }

    fun undo() {
        paintPath.value = paintPath.value.toMutableList().apply {
            undonePath.value = undonePath.value
                .toMutableList().apply {
                    add(paintPath.value.last())
                }.toList()
            this.removeLast()
        }.toList()
    }

    fun getImportBitmap(): Bitmap? {
        return importedBitmap
    }

    fun setImportedImage(bitmap: Bitmap?) {
        importedBitmap = bitmap
    }

    fun redo() {
        paintPath.value = paintPath.value.toMutableList().apply {
            add(undonePath.value.last())
            undonePath.value = undonePath.value
                .toMutableList().apply { removeLast() }.toList()
        }.toList()
    }

    fun reset() {
        undonePath.value = paintPath.value
        paintPath.value = listOf()
    }

    @Composable
    fun clearCanvas() {
        undonePath.value = listOf()
        paintPath.value = listOf()
    }

    fun clearSharedImages() {
        val directoryPath = Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath +
                "/${storageOptions.saveDirectoryName}/${storageOptions.shareDirectoryName}"
        File(directoryPath).deleteRecursively()
    }

}
