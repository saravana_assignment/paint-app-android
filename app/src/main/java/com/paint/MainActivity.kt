package com.paint

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.paint.controller.PainterController
import com.paint.models.StorageOptions
import com.paint.utils.AppConstants.PICK_IMAGE
import com.paint.utils.AppConstants.SHARE_IMAGE
import com.paint.utils.SharedPrefConstant
import com.paint.utils.SharedPreferenceUtils
import com.paint.ui.theme.PaintTheme
import com.paint.controller.rememberCanvasPainterController as rememberCanvasPainterController1

class MainActivity : ComponentActivity() {

    companion object {
        val _bitmapImage = MutableLiveData<Bitmap>()
        private val loadBitmapImage: LiveData<Bitmap> = _bitmapImage
    }
    private lateinit var painterController: PainterController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val permLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it.not()) {
                showPermissionToast()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showPermissionToast()
            } else {
                permLauncher.launch(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }
        setContent {
            SetPaintContent()
        }
    }

    @Composable
    fun SetPaintContent() {
        val loadedImage = loadBitmapImage.observeAsState()
        val sharedPref = SharedPreferenceUtils(this)
        var colorInt = sharedPref.getInt(SharedPrefConstant.PAINT_COLOR)
        var paintColor = if(colorInt != 0){
            Color(colorInt)
        } else {
            Color.Red
        }
        painterController = rememberCanvasPainterController1(
            activity = this,
            maxStrokeWidth = 200f,
            showToolbar = true,
            storageOptions = StorageOptions(shouldSaveByDefault = true),
            color = paintColor,
        )

        painterController.clearCanvas()
        painterController.setImportedImage(loadedImage.value)

        PaintTheme {
            CanvasPainter(
                Modifier
                    .fillMaxSize()
                    .background(Color.White),
                painterController
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            val imageUri = data?.data
            _bitmapImage.value = this.getBitmapFromUri(imageUri)

        } else if (requestCode == SHARE_IMAGE) {
            painterController.clearSharedImages()
        }
    }

    private fun getBitmapFromUri(uri: android.net.Uri?): Bitmap? {
        uri?.let {
            val inputStream = this.contentResolver.openInputStream(it)
            inputStream?.let { stream ->
                return BitmapFactory.decodeStream(stream)
            }
        }
        return null
    }

    private fun showPermissionToast() {
        Toast.makeText(
            applicationContext, "Please Grant Storage Permission in order to save Painting",
            Toast.LENGTH_LONG
        )
            .show()
    }
}