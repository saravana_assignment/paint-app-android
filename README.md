# Paint App Android

Paint App Android
The Paint App Android is a simple drawing application designed for Android devices with Compose. It allows users to create and edit drawings using various tools and colors.

**Features**
Drawing Tools: Includes tools such as pen, eraser, brush, and color selection for creating drawings.

Color Selection: Users can choose from a variety of colors to draw with. For color selection have used godaddy color picker library 

Undo/Redo: Provides undo and redo functionality to correct mistakes or redo actions.

Zoom: User can zoom in zoom out with the fingers

Save and Share: Allows users to save their drawings to the gallery and share them with others.

Customization: Users can customize brush sizes and other parameters to suit their preferences.

Screenshots are available screenshots folder


Installation
Clone the repository to your local machine:

Copy code
git clone https://gitlab.com/saravanarajan.ms/paint-app-android.git
Open the project in Android Studio.

Build and run the project on an Android emulator or a physical device.

**Usage**
Select a drawing tool from the toolbar.

Choose a color from the color palette.

Start drawing on the canvas using your finger or stylus.

Use the eraser tool to erase parts of the drawing.

Adjust brush size as needed.

Save or share your drawing using the options provided.

